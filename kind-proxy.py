import requests
import pandas as pd
import pathlib
import click

URL = "https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list.txt"


def get_euro_proxies(data: list)->list:
    df = pd.DataFrame(data,
                      columns=['ip', 'tags', 'google', 'other'])
    country = df.tags.str.split(
        '-', expand=True).rename(columns={0: 'country'})[['country']]
    data = pd.concat([df, country], axis=1)
    euro = data[data.country.isin(
        ['FR', 'DE', 'IT', 'CZ', 'BY', 'CH', 'ES', 'GH', 'HR'])].ip
    euro = list(euro)
    proxies = ['http://'+elt for elt in euro]
    return proxies


@click.command()
@click.argument('path', type=click.Path(dir_okay=False))
def download(path: str):
    """Write a list of proxy to the path (e.g. my/path/proxy.txt)
    """
    loc = pathlib.Path(path)
    raw = requests.get(URL)
    raw.raise_for_status()
    doc = raw.text.splitlines()[4:-3]
    data = [elt.split(' ') for elt in doc]
    loc.write_text('\n'.join(get_euro_proxies(data)))


if __name__ == "__main__":
    download()
